// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sftp

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/pkg/sftp"
	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/pkg"
	"golang.org/x/crypto/ssh"
)

func ConfigFromUrl(url *url.URL) SshConfig {
	config := SshConfig{
		Addr:    url.Host,
		Path:    url.Path,
		PrivKey: url.Query().Get("key"),
	}
	if !strings.Contains(config.Addr, ":") {
		config.Addr += ":22"
	}
	if url.User != nil {
		config.User = url.User.Username()
	}
	if config.PrivKey == "" {
		homeDir, _ := os.UserHomeDir()
		config.PrivKey = filepath.Join(homeDir, ".ssh/id_rsa")
	}
	return config
}

type SshConfig struct {
	User    string
	Addr    string
	Path    string
	PrivKey string
}

func sftpUploadFile(client *sftp.Client, localPath, remotePath string) error {
	remoteFile, err := client.Create(remotePath)
	if err != nil {
		return err
	}
	defer remoteFile.Close()

	localFile, err := os.Open(localPath)
	if err != nil {
		return err
	}
	defer localFile.Close()

	_, err = io.Copy(remoteFile, localFile)
	return err
}

func loadLocalIndex() (*pkg.ModIndex, error) {
	file, err := os.Open("output/packages/index.json")
	if err != nil {
		return nil, err
	}
	defer file.Close()
	index := new(pkg.ModIndex)
	if err := json.NewDecoder(file).Decode(index); err != nil {
		return nil, err
	}
	return index, nil
}

func loadRemoteIndex(client *sftp.Client, filepath string) (*pkg.ModIndex, error) {
	file, err := client.Open(path.Join(filepath, "index.json"))
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}
	defer file.Close()
	index := new(pkg.ModIndex)
	if err := json.NewDecoder(file).Decode(index); err != nil {
		return nil, err
	}
	return index, nil
}

func remoteFileUpToDate(localIndex, remoteIndex *pkg.ModIndex, filename string) bool {
	if remoteIndex == nil {
		return false
	}
	if filename == "index.json" {
		return false
	}
	var localHash, remoteHash string
	for _, entry := range localIndex.Entries {
		if entry.Name == filename {
			localHash = entry.Checksum
			break
		}
	}
	if localHash == "" {
		return true
	}
	for _, entry := range remoteIndex.Entries {
		if entry.Name == filename {
			remoteHash = entry.Checksum
			break
		}
	}
	return localHash == remoteHash
}

func Publish(c *modbuilder.Context, config SshConfig) ([]string, error) {
	c.LogTask("Uploading mod files over SFTP")

	keyBytes, err := ioutil.ReadFile(config.PrivKey)
	if err != nil {
		return nil, err
	}
	keyBytes = bytes.TrimPrefix(keyBytes, []byte{0xEF, 0xBB, 0xBF})
	key, err := ssh.ParsePrivateKey(keyBytes)
	if err != nil {
		return nil, err
	}
	sshConfig := &ssh.ClientConfig{
		User: config.User,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(key),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	sshClient, err := ssh.Dial("tcp", config.Addr, sshConfig)
	if err != nil {
		return nil, err
	}
	defer sshClient.Close()

	sftpClient, err := sftp.NewClient(sshClient)
	if err != nil {
		return nil, err
	}
	defer sftpClient.Close()

	localIndex, err := loadLocalIndex()
	if err != nil {
		return nil, err
	}

	remoteIndex, err := loadRemoteIndex(sftpClient, config.Path)
	if err != nil {
		return nil, err
	}

	files, err := filepath.Glob("output/packages/*")
	if err != nil {
		return nil, err
	}

	updatedFiles := make([]string, 0)

	for _, file := range files {
		filename := filepath.Base(file)
		if remoteFileUpToDate(localIndex, remoteIndex, filename) {
			continue
		}
		updatedFiles = append(updatedFiles, filename)
		c.Logf("Uploading file %q", filename)
		remotePath := path.Join(config.Path, filename+".mbd-atomic")
		if err := sftpUploadFile(sftpClient, file, remotePath); err != nil {
			return nil, err
		}
	}

	for _, file := range updatedFiles {
		oldPath := path.Join(config.Path, filepath.Base(file)+".mbd-atomic")
		newPath := path.Join(config.Path, filepath.Base(file))
		if err := sftpClient.PosixRename(oldPath, newPath); err != nil {
			return nil, err
		}
	}

	return updatedFiles, nil
}
