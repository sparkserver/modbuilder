// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package pkg

import (
	"path/filepath"
	"time"
)

type ModIndexEntry struct {
	Name     string
	Checksum string
	Size     int64
}

type ModIndex struct {
	BuiltAt string          `json:"built_at"`
	Entries []ModIndexEntry `json:"entries"`
}

func WriteModIndex() error {
	packages, err := filepath.Glob("output/packages/*.mods")
	if err != nil {
		return err
	}
	entries := make([]ModIndexEntry, len(packages))
	for i, pkg := range packages {
		hash, size, err := hashFile(pkg)
		if err != nil {
			return err
		}
		entries[i] = ModIndexEntry{
			Name:     filepath.Base(pkg),
			Checksum: hash,
			Size:     size,
		}
	}
	index := ModIndex{
		BuiltAt: time.Now().UTC().Format(time.RFC3339),
		Entries: entries,
	}
	return fileWriteJson("output/packages/index.json", index)
}
