// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package pkg

import (
	"encoding/json"
	"os"
	"path/filepath"

	"gitlab.com/sparkserver/modbuilder/pkg/packager"
)

func WritePackagerConfig(updatedPackages []*PackageStruct) error {
	pkgDefs := make([]packager.PackageDefinition, len(updatedPackages))
	for i, pkg := range updatedPackages {
		pkgDefs[i] = packager.PackageDefinition{
			SourceName:       pkg.Name,
			DistributionName: pkg.Name,
		}
		if err := os.MkdirAll(".mbd/package-temp/src/"+pkg.Name, os.ModePerm); err != nil {
			return err
		}
		entries := make([]packager.PackageEntry, len(pkg.Entries))
		for i, entry := range pkg.Entries {
			absSrcPath, err := filepath.Abs("output/files/" + entry.Path)
			if err != nil {
				return err
			}
			entries[i] = packager.PackageEntry{
				Type:      entry.PackagerType(),
				LocalPath: absSrcPath,
				GamePath:  entry.Path,
			}
		}
		config := packager.PackageConfig{
			EncryptFiles: false,
			Entries:      entries,
		}
		if err := fileWriteJson(".mbd/package-temp/src/"+pkg.Name+"/config.json", config); err != nil {
			return err
		}
	}
	config := packager.Config{
		GenerateIndex: false,
		Packages:      pkgDefs,
	}
	return fileWriteJson(".mbd/package-temp/config.json", config)
}

func fileWriteJson(filename string, data interface{}) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	return json.NewEncoder(file).Encode(data)
}
