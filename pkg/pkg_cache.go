// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package pkg

import (
	"encoding/json"
	"os"
)

type PackageCache []*PackageStruct

func LoadPackageCache() (PackageCache, error) {
	f, err := os.Open(".mbd/cache-v1/package-entries.json")
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}
	defer f.Close()

	var cache PackageCache
	err = json.NewDecoder(f).Decode(&cache)
	return cache, err
}

func SavePackageCache(cache PackageCache) error {
	if err := os.MkdirAll(".mbd/cache-v1", os.ModePerm); err != nil {
		return err
	}
	f, err := os.Create(".mbd/cache-v1/package-entries.json")
	if err != nil {
		return err
	}
	defer f.Close()

	return json.NewEncoder(f).Encode(cache)
}

func CachePackageChanged(cache PackageCache, pkg *PackageStruct) bool {
	for _, cp := range cache {
		if cp.Name == pkg.Name {
			return !CachePackagesEqual(cp, pkg)
		}
	}
	return true
}

func CachePackagesEqual(a *PackageStruct, b *PackageStruct) bool {
	aEntries := make(map[string]PackageEntryType)
	for _, ae := range a.Entries {
		aEntries[ae.Path] = ae.Type
	}

	for _, be := range b.Entries {
		ae, ok := aEntries[be.Path]
		if !ok {
			// B has entry that A doesn't have
			return false
		}

		if ae != be.Type {
			// Entry with same path has different type
			return false
		}

		delete(aEntries, be.Path)
	}

	if len(aEntries) > 0 {
		// A has entries that B doesn't have
		return false
	}

	return true
}
