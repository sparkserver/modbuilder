// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package pkg

import (
	"crypto/sha1"
	"encoding/hex"
	"io"
	"os"

	"github.com/minio/highwayhash"
)

func hashFile(path string) (string, int64, error) {
	hash := sha1.New()
	file, err := os.Open(path)
	if err != nil {
		return "", 0, err
	}
	defer file.Close()
	size, err := io.Copy(hash, file)
	if err != nil {
		return "", 0, err
	}
	return hex.EncodeToString(hash.Sum(nil)), size, nil
}

func hashFileName(name string) uint64 {
	hashKey := []byte{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
		0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
	}
	return highwayhash.Sum64([]byte(name), hashKey)
}
