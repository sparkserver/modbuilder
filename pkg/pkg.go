// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package pkg

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"

	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/cache"
)

func CreateSplitPackages(baseName string, folderPath string, count int) ([]*PackageStruct, error) {
	entries, err := ioutil.ReadDir("output/files/" + folderPath)
	if err != nil {
		return nil, err
	}
	packages := make([]*PackageStruct, count)
	for i := range packages {
		var pkgName string
		if i > 0 {
			pkgName = baseName + strconv.Itoa(i)
		} else {
			pkgName = baseName
		}
		packages[i] = &PackageStruct{
			Name:    pkgName,
			Entries: []PackageEntry{},
		}
	}
	for _, entry := range entries {
		pkgIdx := int(hashFileName(entry.Name()) % uint64(count))
		var entryType PackageEntryType
		if entry.IsDir() {
			entryType = PackageEntryTypeFolder
		} else {
			entryType = PackageEntryTypeFile
		}
		packages[pkgIdx].Entries = append(packages[pkgIdx].Entries, PackageEntry{
			Type: entryType,
			Path: path.Join(folderPath, entry.Name()),
		})
	}
	return packages, nil
}

func CreateAllPackages(c *modbuilder.Context) ([]*PackageStruct, error) {
	packages := make([]*PackageStruct, 0)
	entries, err := ioutil.ReadDir("output/files")
	if err != nil {
		return nil, err
	}
	for _, entry := range entries {
		if !entry.IsDir() {
			c.Warn("Mod files have %q in root, packaging it isn't supported!", entry.Name())
			continue
		}
		pkgName := strings.ToLower(entry.Name())
		override := c.Config.Packages[pkgName]
		if override.SplitCount > 0 {
			splits, err := CreateSplitPackages(pkgName, entry.Name(), override.SplitCount)
			if err != nil {
				return nil, err
			}
			packages = append(packages, splits...)
		} else {
			packages = append(packages, &PackageStruct{
				Name: pkgName,
				Entries: []PackageEntry{
					{
						Type: PackageEntryTypeFolder,
						Path: entry.Name(),
					},
				},
			})
		}
	}
	return packages, nil
}

func FilterChangedPackages(packages []*PackageStruct, changes cache.Changes, pkgCache PackageCache) []*PackageStruct {
	out := make([]*PackageStruct, 0, len(packages))
	for _, pkg := range packages {
		if !pkg.DoesExist() {
			fmt.Printf("Building package %s because it doesn't exist in output\n", pkg.Name)
			out = append(out, pkg)
			continue
		}

		if pkg.HasChanged(changes) {
			fmt.Printf("Building package %s because its source files changed\n", pkg.Name)
			out = append(out, pkg)
			continue
		}

		if CachePackageChanged(pkgCache, pkg) {
			fmt.Printf("Building package %s because its file list changed\n", pkg.Name)
			out = append(out, pkg)
			continue
		}
	}
	return out
}

func Package(c *modbuilder.Context) error {
	c.LogTask("Packaging mod files")

	pkgCache, err := LoadPackageCache()
	if err != nil {
		return err
	}

	packages, err := CreateAllPackages(c)
	if err != nil {
		return err
	}

	updated := FilterChangedPackages(packages, c.Changes, pkgCache)

	packageNames := make([]string, len(updated))
	for i, pkg := range updated {
		packageNames[i] = pkg.Name
	}

	if len(updated) == 0 {
		return WriteModIndex()
	}

	c.Logf("%v packages to build: %s", len(updated), strings.Join(packageNames, ","))

	if err := os.RemoveAll(".mbd/package-temp"); err != nil {
		return err
	}

	if err := WritePackagerConfig(updated); err != nil {
		return err
	}

	if err := os.MkdirAll("output/packages", os.ModePerm); err != nil {
		return err
	}

	cmd := exec.Command(c.ExecPath("ModPackager"), "-i", ".mbd/package-temp/config.json", "-o", "output/packages")
	if output, err := cmd.CombinedOutput(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("command failed:\n%v", string(output))
		}
		return err
	}

	if err := SavePackageCache(packages); err != nil {
		return err
	}

	return WriteModIndex()
}
