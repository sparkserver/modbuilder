// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package pkg

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/sparkserver/modbuilder/cache"
)

type PackageEntryType uint

const (
	PackageEntryTypeFile   PackageEntryType = iota
	PackageEntryTypeFolder                  = iota
)

type PackageStruct struct {
	Name    string
	Entries []PackageEntry
}

func (p PackageStruct) HasChanged(changes cache.Changes) bool {
	for _, e := range p.Entries {
		if e.HasChanged(changes) {
			return true
		}
	}
	return false
}

func (p PackageStruct) DoesExist() bool {
	_, err := os.Stat("output/packages/" + p.Name + ".mods")
	return err == nil
}

type PackageEntry struct {
	Type PackageEntryType
	Path string
}

func (e PackageEntry) PackagerType() string {
	switch e.Type {
	case PackageEntryTypeFile:
		return "File"
	case PackageEntryTypeFolder:
		return "Directory"
	default:
		panic(fmt.Errorf("Invalid package entry type: %d", e.Type))
	}
}

func (e PackageEntry) HasChanged(changes cache.Changes) bool {
	var changeCheck func(string, string) bool
	switch e.Type {
	case PackageEntryTypeFile:
		changeCheck = fileChangeCheck
	case PackageEntryTypeFolder:
		changeCheck = folderChangeCheck
	default:
		panic(fmt.Errorf("Invalid package entry type: %d", e.Type))
	}
	for _, c := range changes {
		if changeCheck(c.Path, e.Path) {
			return true
		}
	}
	return false
}

func fileChangeCheck(changePath string, entryPath string) bool {
	return changePath == "output/files/"+entryPath
}

func folderChangeCheck(changePath string, entryPath string) bool {
	return strings.HasPrefix(changePath, "output/files/"+entryPath+"/")
}
