// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package packager

type PackageDefinition struct {
	SourceName       string `json:"source_name"`
	DistributionName string `json:"distribution_name"`
}

type Config struct {
	GenerateIndex bool                `json:"generate_index"`
	Packages      []PackageDefinition `json:"packages"`
}

type PackageEntry struct {
	Type      string `json:"type"`
	LocalPath string `json:"local_path"`
	GamePath  string `json:"game_path"`
}

type PackageConfig struct {
	EncryptFiles bool           `json:"encrypt_files"`
	Entries      []PackageEntry `json:"entries"`
}
