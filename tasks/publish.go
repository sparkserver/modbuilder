// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package tasks

import (
	"net/url"

	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/cf"
	"gitlab.com/sparkserver/modbuilder/sftp"
)

func Publish(ctx *modbuilder.Context, urlString string, cfOpts cf.PurgeOptions) error {
	url, err := url.Parse(urlString)
	if err != nil {
		return err
	}

	updated, err := sftp.Publish(ctx, sftp.ConfigFromUrl(url))
	if err != nil {
		return err
	}

	if cfOpts.Base != "" && cfOpts.Token != "" && cfOpts.Zone != "" {
		return cf.PurgeCache(ctx, cfOpts, updated)
	}

	return nil
}
