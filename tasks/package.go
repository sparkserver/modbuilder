// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package tasks

import (
	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/cache"
	"gitlab.com/sparkserver/modbuilder/pkg"
)

func Package(ctx *modbuilder.Context) error {
	oldCache, err := cache.LoadCache("package-input")
	if err != nil {
		return err
	}
	newCache, err := cache.CreateCache("output/files")
	if err != nil {
		return err
	}
	ctx.Changes = cache.CompareCaches(oldCache, newCache)

	if err := pkg.Package(ctx); err != nil {
		return err
	}

	if err := cache.SaveCache("package-input", newCache); err != nil {
		return err
	}

	return nil
}
