// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package tasks

import (
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/cache"
	"gitlab.com/sparkserver/modbuilder/lang"
	"gitlab.com/sparkserver/modbuilder/vlt"
)

func Build(ctx *modbuilder.Context) error {
	oldCache, err := cache.LoadCache("build-input")
	if err != nil {
		return err
	}
	newCache, err := cache.CreateCache("languages", "base")
	if err != nil {
		return err
	}
	ctx.Changes = cache.CompareCaches(oldCache, newCache)

	if err := copyBaseFiles(ctx); err != nil {
		return err
	}

	if err := lang.Build(ctx); err != nil {
		return err
	}

	if err := vlt.Build(ctx); err != nil {
		return err
	}

	if err := cache.SaveCache("build-input", newCache); err != nil {
		return err
	}

	return nil
}

func copyBaseFiles(ctx *modbuilder.Context) error {
	ctx.LogTask("Copying base files")
	err := filepath.Walk("base", func(wpath string, info os.FileInfo, err error) error {
		if err != nil {
			if wpath == "base" {
				return nil
			}
			return err
		}
		if !info.IsDir() {
			srcPath := filepath.ToSlash(wpath)
			outPath := "output/files/" + srcPath[5:] // remove base/ prefix
			if err := os.MkdirAll(path.Dir(outPath), os.ModePerm); err != nil {
				return err
			}
			if err := copyFileIfNeeded(ctx.Changes, srcPath, outPath); err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return err
	}
	for _, change := range ctx.Changes {
		if change.Type == cache.ChangeTypeRemoved && strings.HasPrefix(change.Path, "base/") {
			outPath := "output/files/" + change.Path[5:] // remove base/ prefix
			ctx.Logf("Removing %s", outPath)
			if err := os.Remove(outPath); err != nil {
				return err
			}
		}
	}
	return nil
}

func copyFileIfNeeded(changes cache.Changes, src, dst string) error {
	if _, err := os.Stat(dst); err != nil {
		if os.IsNotExist(err) {
			// dst doesn't exist -> copy
			return copyFile(src, dst)
		}
		return err
	}

	for _, change := range changes {
		if change.Path == src {
			// src changed -> copy
			return copyFile(src, dst)
		}
	}

	return nil
}

func copyFile(a, b string) error {
	fa, err := os.Open(a)
	if err != nil {
		return err
	}
	defer fa.Close()

	fb, err := os.Create(b)
	if err != nil {
		return err
	}
	defer fb.Close()

	_, err = io.Copy(fb, fa)
	return err
}
