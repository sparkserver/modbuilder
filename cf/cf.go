// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cf

import (
	"net/url"

	"github.com/cloudflare/cloudflare-go"
	"gitlab.com/sparkserver/modbuilder"
)

type PurgeOptions struct {
	Token string
	Zone  string
	Base  string
}

func PurgeCache(c *modbuilder.Context, opts PurgeOptions, updated []string) error {
	c.LogTask("Purging CF cache")

	baseURL, err := url.Parse(opts.Base)
	if err != nil {
		return err
	}

	files := make([]string, len(updated))
	for i, file := range updated {
		fileURL, err := url.Parse(file)
		if err != nil {
			return err
		}
		fullURL := baseURL.ResolveReference(fileURL).String()
		c.Logf("Invalidating URL %q", fullURL)
		files[i] = fullURL
	}

	cf, err := cloudflare.NewWithAPIToken(opts.Token, cloudflare.UserAgent("modbuilder (+https://gitlab.com/sparkserver/modbuilder)"))
	if err != nil {
		return err
	}

	_, err = cf.PurgeCache(opts.Zone, cloudflare.PurgeCacheRequest{
		Files: files,
	})
	return err
}
