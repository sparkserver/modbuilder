// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"fmt"
	"github.com/alecthomas/kong"
	"github.com/mattn/go-colorable"
	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/cf"
	"gitlab.com/sparkserver/modbuilder/config"
	"gitlab.com/sparkserver/modbuilder/tasks"
	"gitlab.com/sparkserver/modbuilder/vlt"
	"os"
)

type BuildCmd struct{}

func (cmd *BuildCmd) Run(ctx *modbuilder.Context) error {
	if err := tasks.Build(ctx); err != nil {
		return err
	}
	ctx.Success("All done!")
	return nil
}

type PackageCmd struct{}

func (cmd *PackageCmd) Run(ctx *modbuilder.Context) error {
	if err := tasks.Build(ctx); err != nil {
		return err
	}
	if err := tasks.Package(ctx); err != nil {
		return err
	}
	ctx.Success("All done!")
	return nil
}

type PublishCmd struct {
	CFToken string `name:"cf-token"`
	CFZone  string `name:"cf-zone"`
	CFBase  string `name:"cf-base"`
	URL     string `arg name:"url"`
}

func (cmd *PublishCmd) Run(ctx *modbuilder.Context) error {
	if err := tasks.Build(ctx); err != nil {
		return err
	}
	if err := tasks.Package(ctx); err != nil {
		return err
	}

	cfOpts := cf.PurgeOptions{
		Token: cmd.CFToken,
		Zone:  cmd.CFZone,
		Base:  cmd.CFBase,
	}
	if err := tasks.Publish(ctx, cmd.URL, cfOpts); err != nil {
		return err
	}

	ctx.Success("All done!")
	return nil
}

type ApplyCmd struct {
	Scripts []string `arg name:"file.nfsms" help:"Paths to ModScript files" type:"path"`
}

func (cmd *ApplyCmd) Run(ctx *modbuilder.Context) error {
	return vlt.ApplyModscript(ctx, cmd.Scripts...)
}

var cli struct {
	Build   BuildCmd   `cmd help:"Builds the mod files"`
	Package PackageCmd `cmd help:"Builds and packages the mod files"`
	Publish PublishCmd `cmd help:"Builds, packages and publishes the mod files"`
	Apply   ApplyCmd   `cmd help:"Applies a ModScript file to the VLT database"`
}

func main() {
	cfg, err := config.LoadConfigOrDefault()
	if err != nil {
		fmt.Printf("Failed to load configuration: %v", err)
		os.Exit(2)
	}

	mbCtx := &modbuilder.Context{
		Stdout: colorable.NewColorableStdout(),
		Config: cfg,
	}
	ctx := kong.Parse(&cli, kong.UsageOnError())
	err = ctx.Run(mbCtx)
	ctx.FatalIfErrorf(err)
}
