// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package vlt

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/sparkserver/modbuilder"
)

var (
	modelCollisionRegex = regexp.MustCompile("^update_field pvehicle (.+) MODEL Collision (.+)$")
	modelVisualRegex    = regexp.MustCompile("^(update_field pvehicle .+ MODEL) Visual (.+)$")
	turboSndRegex1      = regexp.MustCompile("^(update_field pvehicle .+ (?:Turbo|Shift)SND\\[.+\\]) ((Class|Collection) .+)$")
	turboSndRegex2      = regexp.MustCompile("^(update_field pvehicle .+ (?:Turbo|Shift)SND\\[.+\\]) Level (.+)$")
)

func processModscriptFile(ctx *modbuilder.Context, outDir string, file string) (string, error) {
	inFile, err := os.Open(file)
	if err != nil {
		return "", err
	}
	defer inFile.Close()

	outPath := filepath.Join(outDir, filepath.Base(file))
	outFile, err := os.Create(outPath)
	if err != nil {
		return "", err
	}
	defer outFile.Close()

	scanner := bufio.NewScanner(inFile)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		submatches := modelCollisionRegex.FindStringSubmatch(line)
		if submatches != nil {
			ctx.Warn("Skipping unsupported MODEL Collision %v entry for pvehicle %v", submatches[2], submatches[1])
			line = "#" + line
			goto write
		}
		line = modelVisualRegex.ReplaceAllString(line, "$1 $2")
		line = turboSndRegex1.ReplaceAllString(line, "$1 ReferencedRow $2")
		line = turboSndRegex2.ReplaceAllString(line, "$1 UpgradeLevel $2")
	write:
		if _, err := outFile.WriteString(line + "\n"); err != nil {
			return "", err
		}
	}

	return outPath, nil
}

func ApplyModscript(c *modbuilder.Context, files ...string) error {
	c.LogTask("Rewriting ModScript files for YAMLDatabase compatibility")

	outDir, err := ioutil.TempDir("", "mbd-modscript-*")
	if err != nil {
		return err
	}
	defer os.RemoveAll(outDir)

	args := []string{"apply-script", "--no-backup", "--no-bins", "-p", "WORLD", "-i", "vlt", "-o", "vlt", "-s"}

	for _, file := range files {
		filePath, err := processModscriptFile(c, outDir, file)
		if err != nil {
			return err
		}
		args = append(args, filePath)
	}

	c.LogTask("Applying ModScript files")

	cmd := exec.Command(c.ExecPath("Attribulator.CLI"), args...)
	if output, err := cmd.CombinedOutput(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("ModScript apply failed: command output:\n%v", string(output))
		}
		return err
	}

	return nil
}
