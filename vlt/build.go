// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package vlt

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/sparkserver/modbuilder"
)

func GetGameOutputPath(group, name string) string {
	switch group {
	case "main":
		return name
	case "gameplay":
		return "gc.vaults/" + name
	default:
		panic("Unknown group " + group)
	}
}

func Build(c *modbuilder.Context) error {
	if _, err := os.Stat("vlt/info.yml"); err != nil {
		// info.yml doesn't exist -> nothing to do, exit early
		return nil
	}

	c.LogTask("Building VLT files")

	if err := os.MkdirAll("output/files/GLOBAL/gc.vaults", os.ModePerm); err != nil {
		return err
	}

	cmd := exec.Command(c.ExecPath("Attribulator.CLI"), "pack", "-p", "WORLD", "-i", "vlt", "-o", ".mbd/vlt-gen", "-c")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("VLT build failed")
		}
		return err
	}

	files, err := filepath.Glob(".mbd/vlt-gen/*/*.bin")
	if err != nil {
		return err
	}

	for _, filePath := range files {
		src := filepath.ToSlash(filePath)
		pathSplits := strings.Split(src, "/")
		group, name := pathSplits[len(pathSplits)-2], pathSplits[len(pathSplits)-1]
		dest := "output/files/GLOBAL/" + GetGameOutputPath(group, name)
		if err := os.Rename(src, dest); err != nil {
			return err
		}
	}

	return nil
}
