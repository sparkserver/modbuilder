module gitlab.com/sparkserver/modbuilder

go 1.12

require (
	github.com/alecthomas/kong v0.2.9
	github.com/cloudflare/cloudflare-go v0.11.7
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/mattn/go-colorable v0.1.6
	github.com/minio/highwayhash v1.0.0
	github.com/pelletier/go-toml/v2 v2.0.0-beta.3
	github.com/pkg/sftp v1.11.0
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
)
