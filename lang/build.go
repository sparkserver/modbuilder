// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package lang

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/sparkserver/modbuilder"
	"gitlab.com/sparkserver/modbuilder/cache"
)

func binName(jsonName string) string {
	return strings.TrimSuffix(jsonName, ".json") + ".bin"
}

func GetUpdatedFiles(changes cache.Changes) ([]string, error) {
	updatedFiles := make([]string, 0)
	entries, err := ioutil.ReadDir("languages")
	if err != nil {
		return nil, err
	}
outer:
	for _, entry := range entries {
		if entry.Name() == "Labels_Global.json" {
			continue
		}
		if !strings.HasSuffix(entry.Name(), "_Global.json") {
			// Not a language file
			continue
		}
		for _, change := range changes {
			if change.Path == "languages/"+entry.Name() {
				updatedFiles = append(updatedFiles, entry.Name())
				continue outer
			}
		}
		if _, err := os.Stat("output/files/LANGUAGES/" + binName(entry.Name())); err != nil {
			// output file doesn't exist -> add to updatedFiles
			updatedFiles = append(updatedFiles, entry.Name())
		}
	}
	return updatedFiles, nil
}

func Build(c *modbuilder.Context) error {
	if _, err := os.Stat("languages"); err != nil {
		// languages doesn't exist -> nothing to do, exit early
		return nil
	}

	c.LogTask("Building language files")

	if err := os.MkdirAll("output/files/LANGUAGES", os.ModePerm); err != nil {
		return err
	}

	updated, err := GetUpdatedFiles(c.Changes)
	if err != nil {
		return err
	}

	c.Logf("%v files to build", len(updated))

	if len(updated) == 0 {
		return nil
	}

	if err := os.MkdirAll(".mbd/lang-in", os.ModePerm); err != nil {
		return err
	}

	updated = append(updated, "Labels_Global.json")
	defer os.RemoveAll(".mbd/lang-in")
	for _, file := range updated {
		path := "languages/" + file
		newPath := ".mbd/lang-in/" + file
		if err := os.Link(path, newPath); err != nil {
			return err
		}
	}

	cmd := exec.Command(c.ExecPath("jsontool"), "pack", ".mbd/lang-in", "output/files/LANGUAGES")
	if output, err := cmd.CombinedOutput(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("command failed:\n%v", string(output))
		}
		return err
	}

	return nil
}
