// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package modbuilder

import (
	"fmt"
	"gitlab.com/sparkserver/modbuilder/config"
	"io"
	"runtime"

	. "github.com/logrusorgru/aurora"
	"gitlab.com/sparkserver/modbuilder/cache"
)

type Context struct {
	Stdout  io.Writer
	Changes cache.Changes
	Config *config.Config
}

func (c *Context) LogTask(task string) {
	c.Logf(Sprintf(Cyan("[*] %s"), task))
}

func (c *Context) Success(format string, a ...interface{}) {
	c.Logf(Sprintf(Green(format), a...))
}

func (c *Context) Warn(format string, a ...interface{}) {
	c.Logf(Sprintf(Yellow("[WARN] "+format), a...))
}

func (c *Context) Logf(format string, a ...interface{}) {
	fmt.Fprintf(c.Stdout, format+"\n", a...)
}

func (c *Context) ExecPath(cmd string) string {
	if runtime.GOOS == "windows" {
		return cmd + ".exe"
	}
	return cmd
}
