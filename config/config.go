// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package config

import (
	"fmt"
	"github.com/pelletier/go-toml/v2"
	"io/ioutil"
	"os"
)

type PackageOverride struct {
	SplitCount int `toml:"split_count"`
}

type Config struct {
	Packages map[string]PackageOverride `toml:"packages"`
}

var defaultConfig = &Config{}

func LoadConfigOrDefault() (*Config, error) {
	data, err := ioutil.ReadFile("modbuilder.toml")
	if err != nil {
		if os.IsNotExist(err) {
			return defaultConfig, nil
		}
		return nil, err
	}

	fmt.Println("Loaded configuration from modbuilder.toml")

	var config Config
	if err := toml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}
