// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cache

import (
	"os"
	"path/filepath"
)

type Cache map[string]uint64

// CreateCache creates Cache that includes the specified directories.
// If specified directory doesn't exist, it will be ignored.
func CreateCache(paths ...string) (Cache, error) {
	cache := make(Cache)
	for _, rootPath := range paths {
		err := filepath.Walk(rootPath, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				if path == rootPath {
					return nil
				}
				return err
			}
			if !info.IsDir() {
				hash, err := hashFile(path)
				if err != nil {
					return err
				}
				cache[filepath.ToSlash(path)] = hash
			}
			return nil
		})
		if err != nil {
			return nil, err
		}
	}
	return cache, nil
}
