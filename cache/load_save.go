// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cache

import (
	"encoding/json"
	"os"
)

// LoadCache loads cache from disk.
// If there's no cache saved to the disk, an empty Cache will be returned.
func LoadCache(name string) (Cache, error) {
	f, err := os.Open(".mbd/cache-v1/" + name + ".json")
	if err != nil {
		if os.IsNotExist(err) {
			return make(Cache), nil
		}
		return nil, err
	}
	defer f.Close()

	var cache Cache
	err = json.NewDecoder(f).Decode(&cache)
	return cache, err
}

// SaveCache saves cache to disk.
func SaveCache(name string, cache Cache) error {
	if err := os.MkdirAll(".mbd/cache-v1", os.ModePerm); err != nil {
		return err
	}
	f, err := os.Create(".mbd/cache-v1/" + name + ".json")
	if err != nil {
		return err
	}
	defer f.Close()

	return json.NewEncoder(f).Encode(cache)
}
