// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cache

const (
	ChangeTypeModified = iota
	ChangeTypeAdded
	ChangeTypeRemoved
)

type Change struct {
	Type int
	Path string
}

type Changes []Change

func CompareCaches(old, new Cache) Changes {
	changes := make(Changes, 0)
	for path, oldHash := range old {
		newHash, ok := new[path]
		if !ok {
			changes = append(changes, Change{
				Type: ChangeTypeRemoved,
				Path: path,
			})
		} else if oldHash != newHash {
			changes = append(changes, Change{
				Type: ChangeTypeModified,
				Path: path,
			})
		}
	}
	for path := range new {
		if _, ok := old[path]; !ok {
			changes = append(changes, Change{
				Type: ChangeTypeAdded,
				Path: path,
			})
		}
	}
	return changes
}
